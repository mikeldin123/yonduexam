//
//  ViewController.m
//  YonduExam
//
//  Created by Lenie Noi on 03/06/2016.
//  Copyright © 2016 Mac. All rights reserved.
//


#import "ViewController.h"

#import "LoadingView.h"

@interface ViewController ()
{
    NSMutableArray *arrItems;
    
    LoadingView *loadingView;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    arrItems=[[NSMutableArray alloc] init];
    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSArray *arr = [YEHTTPRequest loadRSSFeedLinks];
    
    
    loadingView = [[LoadingView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:loadingView];
    
    [YEHTTPRequest loadRSSFeedLinks:arr withLoadCompletion:^(NSArray *rssObject) {
        
        [arrItems addObjectsFromArray:rssObject];
        
    } withFeedListCompletion:^(BOOL success){
        
        if (success==NO) {
            
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Exam"
                                                                            message:@"No Internet connection"
                                                                     preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction * okBTN = [UIAlertAction actionWithTitle:@"OK"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action){
        
                                     }];
            
            [alert addAction:okBTN];
            
            
            [self presentViewController:alert animated:YES completion:nil];
            
            [loadingView removeFromSuperview];
            
            return;
        }
        
        YESearchController *searchController = [[YESearchController alloc] initWithNibName:nil bundle:nil];
        [searchController setRssFeedItems:arrItems];
        [self presentViewController:searchController animated:YES completion:^{
            
            [searchController check];
            
        }];
        
        
  
    }];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
