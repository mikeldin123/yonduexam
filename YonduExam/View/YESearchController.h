//
//  YESearchController.h
//  YonduExam
//
//  Created by Lenie Noi on 03/06/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YESearchController : UIViewController


@property (nonatomic, strong) NSMutableArray *rssFeedItems;

- (void)check;

@end
