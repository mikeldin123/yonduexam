//
//  YESearchController.m
//  YonduExam
//
//  Created by Lenie Noi on 03/06/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "YESearchController.h"
#import "RSSObject.h"
#import "YESearchControllerCell.h"

@interface YESearchController () <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
{
    UITableView *resultTBL;
    
    UISearchBar *titleSearchBar;
    
    NSMutableArray *resultsArray;
    
    UIView *searchArea;
    
    BOOL isSearching;
    
}
@end

@implementation YESearchController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        
        resultsArray = [[NSMutableArray alloc] init];
        
        isSearching=NO;
        
        [self searchArea];
        
        resultTBL = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(searchArea.frame), self.view.frame.size.width, self.view.frame.size.height-50) style:UITableViewStylePlain];
        [resultTBL setDelegate:self];
        [resultTBL setDataSource:self];
        resultTBL.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        [self.view addSubview:resultTBL];
        
    }
    
    return self;
}

- (void)searchArea
{
    searchArea = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 70)];
    [searchArea setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:searchArea];
    
    titleSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, searchArea.frame.size.width-50, searchArea.frame.size.height)];
    titleSearchBar.barTintColor = [UIColor redColor];
    titleSearchBar.placeholder = @"Search Book Title";
    [titleSearchBar setDelegate:self];
    [searchArea addSubview:titleSearchBar];
}

- (void)check
{
    NSLog(@"ITEM COUNT ----> %lu",[_rssFeedItems count]);
    
//    [resultTBL reloadData];
    
}

#pragma mark- PREDICATE SEARCHING
- (void)searchForBookTitle:(NSString *)title
{
    
    [resultsArray removeAllObjects];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title BEGINSWITH[cd] %@ && rating > 4",title];
    
    NSArray *searched = [_rssFeedItems filteredArrayUsingPredicate:predicate];
    
    if ([searched count]==0) {
        
        predicate = [NSPredicate predicateWithFormat:@"title BEGINSWITH[cd] %@ && rating == 4",title];
        
        searched = [_rssFeedItems filteredArrayUsingPredicate:predicate];
        
    }
    
    if ([searched count]!=0) {
        
        
        [resultsArray addObjectsFromArray:searched];
        
    }

    [resultTBL reloadData];
    
}

#pragma mark--


#pragma mark- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}
#pragma mark- UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (isSearching==YES&&[resultsArray count]==0) {
        return 1;
    }
    
    return [resultsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    
    YESearchControllerCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        
        cell = [[YESearchControllerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    if ([resultsArray count]==0) {
        
        [cell noResults];
        
    }else{
        
        RSSObject *rssOBJ = (RSSObject *)[resultsArray objectAtIndex:indexPath.row];
        
        [cell loadRSSFeed:rssOBJ];
    }
    
    
  
    
    return cell;
}


#pragma mark- UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
//    NSLog(@"NO SEARCHING.... [%@]",searchText);
    isSearching=YES;
    
    [self searchForBookTitle:searchText];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"SEARCH BUTTON ... %@",searchBar.text);
    
    [self searchForBookTitle:searchBar.text];
    
    isSearching=YES;
    
    [searchBar resignFirstResponder];
    [searchBar endEditing:YES];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    NSLog(@"NAG END....");
    
//    isSearching=NO;
    
    return YES;
}

@end
