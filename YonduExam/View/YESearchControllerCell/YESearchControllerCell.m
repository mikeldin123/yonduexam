//
//  YESearchControllerCell.m
//  YonduExam
//
//  Created by Lenie Noi on 04/06/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "YESearchControllerCell.h"

#import "UIImageView+WebCache.h"

@interface YESearchControllerCell ()
{
    UILabel *title;
    
    UIImage *thumbNail;
    
    UILabel *rating;
    
    UIImageView *thumbView;
    
    UILabel *prodDate;
}
@end

@implementation YESearchControllerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        title = [[UILabel alloc] init];
        [title setFont:[UIFont boldSystemFontOfSize:15]];
        [title setNumberOfLines:2];
        [title setBackgroundColor:[UIColor clearColor]];
        [title setTextColor:[UIColor blackColor]];
        [self addSubview:title];
        
        
        rating = [[UILabel alloc] init];
        [rating setFont:[UIFont systemFontOfSize:13]];
        [rating setBackgroundColor:[UIColor clearColor]];
        [rating setTextColor:[UIColor blackColor]];
        [self addSubview:rating];
        
        
        prodDate = [[UILabel alloc] init];
        [prodDate setFont:[UIFont systemFontOfSize:13]];
        [prodDate setBackgroundColor:[UIColor clearColor]];
        [prodDate setTextColor:[UIColor blackColor]];
        [self addSubview:prodDate];
        
        thumbView = [[UIImageView alloc] init];
        [thumbView setBackgroundColor:[UIColor redColor]];
        [self addSubview:thumbView];
    }
    
    return self;
}

- (void)loadRSSFeed:(RSSObject *)rssOBJ
{
    [thumbView setFrame:CGRectMake(5, (100/2)-(80/2), 60, 80)];
    
    [title setFrame:CGRectMake(CGRectGetMaxX(thumbView.frame)+5, 5, self.contentView.frame.size.width-(thumbView.frame.size.width+10), 40)];
    [title setTextAlignment:NSTextAlignmentLeft];
    [title setText:rssOBJ.title];
    
    [prodDate setFrame:CGRectMake(title.frame.origin.x, CGRectGetMaxY(title.frame)+2, title.frame.size.width, 20)];
    [prodDate setText:rssOBJ.pubDate];
    
    
    [rating setFrame:CGRectMake(title.frame.origin.x, CGRectGetMaxY(prodDate.frame)+2, title.frame.size.width, 20)];
    [rating setText:[NSString stringWithFormat:@"Rating: %.1f",rssOBJ.rating]];
    
    NSURL *url = [NSURL URLWithString:rssOBJ.imgURL];
    
    [thumbView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"no_image.jpg"]];
    
}

- (void)noResults
{
    [thumbView setFrame:CGRectZero];
    [prodDate setFrame:CGRectZero];
    [rating setFrame:CGRectZero];
    
    [title setFrame:CGRectMake(0, (100/2)-(40/2), self.contentView.frame.size.width, 40)];
    [title setText:@"NO SEARCH RESULTS"];
    [title setTextAlignment:NSTextAlignmentCenter];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
