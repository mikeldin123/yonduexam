//
//  YESearchControllerCell.h
//  YonduExam
//
//  Created by Lenie Noi on 04/06/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RSSObject.h"


@interface YESearchControllerCell : UITableViewCell
- (void)loadRSSFeed:(RSSObject *)rssOBJ;
- (void)noResults;
@end
