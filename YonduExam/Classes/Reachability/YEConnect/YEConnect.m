//
//  YEConnect.m
//  YonduExam
//
//  Created by Lenie Noi on 04/06/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "YEConnect.h"

@implementation YEConnect

+(BOOL)canConnect
{
//    NetworkStatus reachable=[[Reachability sharedReachability] internetConnectionStatus];
    
    Reachability *reachable = [Reachability reachabilityForInternetConnection];
    
//    NetworkSt
    BOOL canConnect = [reachable isReachable];
    
    if (canConnect==NO) {
        
        return NO;
    }
    
    return YES;
}


@end
