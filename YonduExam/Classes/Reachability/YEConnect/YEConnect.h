//
//  YEConnect.h
//  YonduExam
//
//  Created by Lenie Noi on 04/06/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Reachability.h"

@interface YEConnect : NSObject
+(BOOL)canConnect;
@end
