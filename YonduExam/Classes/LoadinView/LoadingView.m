//
//  LoadingView.m
//  YonduExam
//
//  Created by Lenie Noi on 04/06/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "LoadingView.h"

#define indicator_size 150

@implementation LoadingView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake((frame.size.width/2)-(indicator_size/2), (frame.size.height/2)-(indicator_size/2), indicator_size, indicator_size)];
        [bgView setBackgroundColor:[UIColor colorWithHue:1 saturation:1 brightness:0 alpha:.5]];
        [self addSubview:bgView];
        
        UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activity.frame = CGRectMake((indicator_size/2)-(50/2), ((indicator_size/2)-(50/2))-10, 50, 50);
        [activity startAnimating];
        [bgView addSubview:activity];
    
        UILabel *lbl=[[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(activity.frame), bgView.frame.size.width, 20)];
        [lbl setBackgroundColor:[UIColor clearColor]];
        [lbl setTextColor:[UIColor whiteColor]];
        [lbl setText:@"Loading RSS"];
        [lbl setTextAlignment:NSTextAlignmentCenter];
        [bgView addSubview:lbl];
    
    }
    return self;
}

@end
