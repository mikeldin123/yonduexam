//
//  YEHTTPRequest.m
//  YonduExam
//
//  Created by Lenie Noi on 03/06/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "YEHTTPRequest.h"

#import "XMLDictionary.h"


#import "RSSObject.h"
#import "YERegex.h"

#import "YEConnect.h"

@interface YERSSParser : NSObject


+ (void)parseRSSInfoDictionary:(NSDictionary *)info withParseCompletion:(void(^)(NSArray *rssItems))handler;
@end



@implementation YEHTTPRequest

+ (void)loadRSSFeedLinks:(NSArray *)linkArray
      withLoadCompletion:(void(^)(NSArray *rssObject))completion
  withFeedListCompletion:(void(^)(BOOL success))handler
{
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    __block int counter=0;
    
    if ([YEConnect canConnect]==NO){
        handler(NO);
        return;
    }
    
    for (NSString *links in linkArray) {

        NSURL *url = [NSURL URLWithString:links];
        
        if ([YEConnect canConnect]==NO){
            handler(NO);
            return;
        }
        
        NSURLSessionDataTask *downloadRSS = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {

            NSDictionary *info=[NSDictionary dictionaryWithXMLData:data];
            
            [YERSSParser parseRSSInfoDictionary:info withParseCompletion:^(NSArray *rssItems) {
            
                completion(rssItems);
                
                counter = counter+1;
                
                if (counter==[linkArray count]) {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //UI UI UI
                        handler(YES);
                    });
                    
                }
                
            }];
            
        }];
      
        [downloadRSS resume];
    }
}

+ (NSArray *)loadRSSFeedLinks
{
    NSString *bundle = [[NSBundle mainBundle] pathForResource:@"TEMP_RSS_LIST" ofType:@"txt"];
    
    if ([bundle length]==0) {
        return nil;
    }
    
    NSString *strTxt = [NSString stringWithContentsOfFile:bundle encoding:NSUTF8StringEncoding error:nil];
    
    if (strTxt==nil) {
        
        return nil;
    }
    
    NSArray *arr = [strTxt componentsSeparatedByString:@"|"];
    
    return arr;
}

@end

@implementation YERSSParser

+ (void)parseRSSInfoDictionary:(NSDictionary *)info withParseCompletion:(void(^)(NSArray *rssItems))handler
{
    if (info==nil||![info objectForKey:@"channel"]) {
        handler(nil);
        return;
    }
    
    __block NSMutableArray *rssArr = [NSMutableArray array];
 
    for (id keys in [info allKeys]) {
        
//        NSLog(@"---- %@",keys);
        
        if ([keys isEqualToString:@"channel"]) {
       
            NSDictionary *channel = (NSDictionary *)[info objectForKey:keys];
            
            for (id _keys in [channel allKeys]) {
                
                if ([_keys isEqualToString:@"item"]) {
                    
                    //array
                    NSArray *itemArr = (NSArray *)[channel objectForKey:_keys];
                    
                    
                    for (NSDictionary *dict in itemArr) {
                    
                        __block RSSObject *rssOBJ = [[RSSObject alloc] init];
                        
//                        NSLog(@"ITEM ARRAY -->");
                        /*
                         description
                         guid
                         link
                         pubDate
                         title
                         */
                        
                        if ([dict objectForKey:@"description"]) {
                            
//                            NSLog(@"DESC ---> %@",[dict objectForKey:@"description"]);
                            [self parseDescription:[dict objectForKey:@"description"]
                                      withRSObject:rssOBJ
                               withParseCompletion:^(RSSObject *rss) {
                                
                            }];
                            
                        }
                        
                        if ([dict objectForKey:@"pubDate"]) {
                            
                            [rssOBJ setPubDate:[dict objectForKey:@"pubDate"]];
                        }
                        
                        
                        if ([dict objectForKey:@"title"]) {
                            
                            NSString *title = [YERegex cleanUpBookTitle:[dict objectForKey:@"title"]];
                            
                            [rssOBJ setTitle:title];
                        }
            
                        [rssArr addObject:rssOBJ];
                    }
                }
            
            }
            
            
        }
    }
    
    handler(rssArr);
    
}

+ (void)parseDescription:(NSString *)desc withRSObject:(RSSObject *)rsOBJ withParseCompletion:(void(^)(RSSObject *rss))handler
{
    if ([desc length]==0) {
        return;
    }
    
    [YERegex parseHTMLBody:desc withParseCompletion:^(NSArray *imgArr) {
       
//        NSLog(@"IMAGE ARRAY ----> %@",imgArr);
        for (NSString *link in imgArr) {
            
            BOOL isThumbnail  = [self checkIfThumbnail:link];
            
            if (isThumbnail==NO) {
             
                float rating = [self getRatingFromLink:link];
                
                [rsOBJ setRating:rating];
                
            }else{
                //save as thumbnail...
                [rsOBJ setImgURL:link];
            }
        }
        
        
    }];
    
}

+ (BOOL)checkIfThumbnail:(NSString *)item
{
    if ([item hasSuffix:@"gif"]) {
        
        return NO;
    }
    
    return YES;
}

+ (float)getRatingFromLink:(NSString *)link
{
    NSArray *linkComp = (NSArray *)[link componentsSeparatedByString:@"/"];
    
    if ([linkComp count]==0) {
        return 0;
    }
    
    NSString *rateSTR = (NSString *)[linkComp lastObject];
    
    NSString *star = [[[rateSTR componentsSeparatedByString:@"."] firstObject] stringByReplacingOccurrencesOfString:@"stars-" withString:@""];
    
    star = [star stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    
    return [star floatValue];
}


@end

