//
//  YEHTTPRequest.h
//  YonduExam
//
//  Created by Lenie Noi on 03/06/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YEHTTPRequest : NSObject


+ (NSArray *)loadRSSFeedLinks;

+ (void)loadRSSFeedLinks:(NSArray *)linkArray
      withLoadCompletion:(void(^)(NSArray *rssObject))completion
  withFeedListCompletion:(void(^)(BOOL success))handler;

@end
