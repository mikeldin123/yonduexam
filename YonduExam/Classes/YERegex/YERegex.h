//
//  YERegex.h
//  YonduExam
//
//  Created by Lenie Noi on 03/06/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YERegex : NSObject

+ (void)parseHTMLBody:(NSString *)htmlStr withParseCompletion:(void(^)(NSArray *imgArr))handler;


+ (NSString *)cleanUpBookTitle:(NSString *)title;
@end
