//
//  YERegex.m
//  YonduExam
//
//  Created by Lenie Noi on 03/06/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "YERegex.h"

@implementation YERegex

+ (void)parseHTMLBody:(NSString *)htmlStr withParseCompletion:(void(^)(NSArray *imgArr))handler
{
    NSError *error = nil;
    //@"(<img\\s[\\s\\S]*?src\\s*?=\\s*?['\"](.*?)['\"][\\s\\S]*?>)+?"
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(<img\\s[\\s\\S]*?src\\s*?=\\s*?['\"](.*?)['\"][\\s\\S]*?>)+?"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    
    if (error) {
        NSLog(@"ERROR DESC ---> %@",[error description]);
        
        handler(nil);
        
        return;
    }
    
    
    __block NSMutableArray *tempArr=[NSMutableArray array];
    
    [regex enumerateMatchesInString:htmlStr
                            options:0
                              range:NSMakeRange(0, [htmlStr length])
                         usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                             
                             NSString *img = [htmlStr substringWithRange:[result rangeAtIndex:2]];
//                             NSLog(@"img src %@",img);
                             
                             [tempArr addObject:img];
                             
                         }];
    
    
    handler(tempArr);
    
}

+ (NSString *)cleanUpBookTitle:(NSString *)title
{
    NSString *cleaned = [title stringByReplacingOccurrencesOfString:@"^#[0-9]:\\s"
                                                       withString:@""
                                                          options:NSRegularExpressionSearch
                                                            range:NSMakeRange(0, title.length)];
    
    return cleaned;
}

@end
