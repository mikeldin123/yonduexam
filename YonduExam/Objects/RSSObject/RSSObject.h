//
//  RSSObject.h
//  YonduExam
//
//  Created by Lenie Noi on 03/06/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSSObject : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic) float rating;
@property (nonatomic, strong) NSString *imgURL;
@property (nonatomic, strong) NSString *pubDate;


@end
